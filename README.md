# Cross-platform-app: macOS Client

This example-implementation shows how to use a cross-platform codebase written in C++ in a Swift-based macOS-application.

The template therefore provides 3 different demos:

1. The basic usage of the [C++-library](https://gitlab.com/cross-platform-app/core-template) in a Swift project.
2. The rendering of a [C++-based OpenGL (ES) implementation](https://gitlab.com/cross-platform-app/gl-example).
3. An example how to access a System-API, like the device-location via the [C++-library](https://gitlab.com/cross-platform-app/core-template).

## Build Dependencies
- XCode >= 9
- cmake >= 3.7.1

**Important:** Make sure to also meet the dependencies in the submodules `Core` and `gl-example`

## Build Instructions

1. Initialize all submodules with `git submodule update --init --recursive`

2. Configure XCode-Project from CMake with `cmake -H. -B_builds -GXcode`

3. open XCode-Project (located in `_builds`), select _macOSClient_ as target and click "run"

## Documentation
The code-documentation is following the guidelines for comments [from Apple](https://developer.apple.com/library/content/documentation/Xcode/Reference/xcode_markup_formatting_ref/index.html#//apple_ref/doc/uid/TP40016497-CH2-SW1).
This does not generate a Doxygen-like html-documentation, but XCode can generate some nice output for the _Quick Help_.

## Screenshots

![Screenshots of the Demo](screenshots.png)

## Known Issues
### OpenGL
The OpenGL demo has implemented high-resolution support. This works when you first visit the OpenGL-Tab. On every future visit the Rendering falls back to low-resolution rendering. If the window gets resized, high-res support is activated again.
Since the calculation of the correct amount of pixels is done by a function provided by the System API, this seems to be an internal issue. Further investigation is needed.