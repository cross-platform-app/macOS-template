
import Cocoa

/// Controller for the OpenGL-Example view.
/// Doesn't need any logic at the moment, since the logic for rendering has been implemented in the custom view
/// `GlExampleView`.
class OpenGLController: NSViewController {
    
    @IBOutlet weak var openGlExampleView: GlExampleView!
    
    override func viewDidDisappear() {
        openGlExampleView.pause()
    }
    
    override func viewWillAppear() {
        openGlExampleView.resume()
    }
}
