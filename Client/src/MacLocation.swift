import CoreLocation

/// macOS-specific implementation of the `LocationInterface` from the `Core` Logic-Tier.
/// this wraps the system-specific API-calls for getting the device-location.
class MacLocation: NSObject, CRLocationInterface, CLLocationManagerDelegate {

    private var location: CRLocationRecord! = CRLocationRecord.init(lat:0,lon:0)
    private var locationManager: CLLocationManager!
    private var onChangeListener: CROnChangeListener!
    
    
    override init() {
        super.init()
        locationManager = CLLocationManager.init()
        locationManager.delegate = self
    }
    
    func updateLocation(_ onChangeListener: CROnChangeListener?) {
        self.onChangeListener = onChangeListener
        // start requesting location change callbacks from the system
        locationManager.startUpdatingLocation()
    }
    
    func getLocation() -> CRLocationRecord {
        return location
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = CRLocationRecord.init(lat: (locationManager.location?.coordinate.latitude)!, lon: (locationManager.location?.coordinate.longitude)!)
        onChangeListener.onSuccess()
        // since we only want te be notified about location-changes once, we need to tell the system to not
        // call back on future changes
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        onChangeListener.onError()
        // since we only want te be notified about location-changes once, we need to tell the system to not
        // call back on future changes
        locationManager.stopUpdatingLocation()
    }
    
}
