
import Cocoa

/// Controller for the Skia-Example view.
/// Doesn't need any logic at the moment, since the logic for rendering has been implemented in the custom view
/// `SkiaExampleView`.
class SkiaController: NSViewController {

    @IBOutlet weak var skiaGlView: SkiaExampleView!
    
    override func viewDidDisappear() {
        skiaGlView.pause()
    }
    
    override func viewWillAppear() {
        skiaGlView.resume()

    }
}
