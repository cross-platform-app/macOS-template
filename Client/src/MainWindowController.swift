
import Cocoa

class MainWindowController: NSWindowController {
    private var tabViewController: NSTabViewController!

    override func windowDidLoad() {
        super.windowDidLoad()
        tabViewController = contentViewController as? NSTabViewController
    }
    
    @IBAction func onSegmentedCellTabNavigationClicked(_ sender: NSSegmentedCell) {
        // this remote-controls the `TabViewController`.
        // Normally the TabViewController would provide its own Controls for switching between tabs,
        // but controlling the tabs from a `SegmentedCell` in the Toolbar looks wayyyy more fancy.
        tabViewController.selectedTabViewItemIndex = sender.selectedSegment
    }
}
