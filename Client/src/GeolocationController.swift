import Cocoa

/// `OnChangeListener` that calls back into the UI-Thread when new location-data is available.
class OnLocationChangedListener: CROnChangeListener {
    private var controller: GeolocationController!
    
    /// - parameters:
    ///     - controller: the controller-instance that the callbacks should be executed on.
    init(controller: GeolocationController) {
        self.controller = controller
    }
    
    func onSuccess() {
        DispatchQueue.main.async {
            self.controller.setStateUpdateLocationLoading(loading: false)
            self.controller.showLocationError(show: false)
            self.controller.updateLocationDisplay()
        }
    }
    
    func onError() {
        DispatchQueue.main.async {
            self.controller.setStateUpdateLocationLoading(loading: false)
            self.controller.showLocationError(show: true)
        }
    }
}

/// Controller that demonstrates how to consume a system-API like the geolocation through an abstraction-layer
/// provided by the Logic-Tier.
class GeolocationController: NSViewController {
    @IBOutlet weak var buttonUpdateLocation: NSButton!
    @IBOutlet weak var textFieldLatitude: NSTextField!
    @IBOutlet weak var textFieldLongitude: NSTextField!
    @IBOutlet weak var progressIndicatorLocation: NSProgressIndicator!
    @IBOutlet weak var textFieldUpdateLocationError: NSTextField!
    private var locationService: CRLocationServiceInterface!
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        locationService = CRLocationServiceInterface.locationService(MacLocation.init())
        locationService.onLocationChanged(OnLocationChangedListener.init(controller: self))
    }
    
    /// This function gets called by the _Update Location_-Button.
    @IBAction func onButtonUpdateLocationClicked(_ sender: NSButton) {
        setStateUpdateLocationLoading(loading: true)
        DispatchQueue.global(qos: .userInitiated).async {
            self.locationService.updateLocation()
        }
    }
    
    /// Sets the state of the UI-elements related to fetching the device-location.
    /// - parameters:
    ///     - loading:
    ///         The UI can have two states:
    ///         - `true`:   The new location is currently being loaded. This means that the UI waits for
    ///                     a system callback and the user can not interact with the _Update Location_-Button.
    ///                     Additionally a rotating progress-indicator is giving feedback to the user.
    ///         - `false`:  The user can request a new location-update. This means that the button can be clicked.
    func setStateUpdateLocationLoading(loading: Bool) {
        if(loading) {
            buttonUpdateLocation.isEnabled = false
            progressIndicatorLocation.isHidden = false
            progressIndicatorLocation.startAnimation(self)
        } else {
            buttonUpdateLocation.isEnabled = true
            progressIndicatorLocation.isHidden = true
            progressIndicatorLocation.stopAnimation(self)
        }
    }
    
    /// Function that updates the TextFields that display the current location.
    /// Requests the new Location on the `locationService` and renders the values as strings.
    func updateLocationDisplay() {
        let location = locationService.getLocation()
        textFieldLatitude.stringValue = String(location.lat)
        textFieldLongitude.stringValue = String(location.lon)
    }
    
    /// Hides/shows the error-message that indicates that something went wrong while requesting
    /// the location
    /// - parameters:
    ///     - show:
    ///         - `true`: the error-message is visible
    ///         - `false`: the error-message is hidden
    func showLocationError(show: Bool) {
        textFieldUpdateLocationError.isHidden = !show
    }
    
}
