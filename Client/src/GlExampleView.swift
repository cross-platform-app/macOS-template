import Cocoa
import OpenGL.GL3

/// custom view that configures the OpenGL-Context that is neccessary to display the result of
/// the OpenGL-calls in the `gl-example`-libary.
class GlExampleView : NSOpenGLView {
    
    private var glExample: GEGlExampleInterface!
    private var displayLink: CVDisplayLink?
    private var context: NSOpenGLContext!

    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        
        
        let attrs: [NSOpenGLPixelFormatAttribute] = [
            UInt32(NSOpenGLPFAAccelerated),
            UInt32(NSOpenGLPFADoubleBuffer),
            UInt32(NSOpenGLPFAColorSize), UInt32(24),
            UInt32(NSOpenGLPFAAlphaSize), UInt32(8),
            UInt32(NSOpenGLPFADepthSize), UInt32(16),
            UInt32(NSOpenGLPFAOpenGLProfile), UInt32(NSOpenGLProfileVersion3_2Core),
            UInt32(NSOpenGLPFANoRecovery),
            UInt32(0)
        ]

        self.pixelFormat = NSOpenGLPixelFormat(attributes: attrs)
        context = NSOpenGLContext(format: self.pixelFormat!, share: nil)
        self.openGLContext = context
        
        self.openGLContext?.setValues([1], for: .swapInterval)
        self.wantsBestResolutionOpenGLSurface = true
    }

    
    override func prepareOpenGL() {
        super.prepareOpenGL()

        glExample = GEGlExampleInterface.glExample()
        glExample.setColor(0.93, green: 0.93, blue: 0.93)
        
        /// the following code is needed for making a rendering loop, that is more efficient than a simple loop.
        /// The controller registers for a callback in the `CVDisplayLink`, when a refresh is needed.
        /// This makes the calculation of the new image stay in sync with the displays refresh-rate.
        /// For more information on this, have a look at these uselful links:
        /// - [Apple Documentation (Objective-C only)](https://developer.apple.com/library/content/qa/qa1385/_index.html)
        /// - [Stachoverflow-discussion on Swift implementation, source for this implementation](https://stackoverflow.com/questions/25981553/cvdisplaylink-with-swift)
        let displayLinkOutputCallback: CVDisplayLinkOutputCallback = {(displayLink: CVDisplayLink, inNow: UnsafePointer<CVTimeStamp>, inOutputTime: UnsafePointer<CVTimeStamp>, flagsIn: CVOptionFlags, flagsOut: UnsafeMutablePointer<CVOptionFlags>, displayLinkContext: UnsafeMutableRawPointer?) -> CVReturn in

            let view = unsafeBitCast(displayLinkContext, to: GlExampleView.self)
            DispatchQueue.main.async {
                view.display()
            }
            return kCVReturnSuccess
        }
        
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink)
        CVDisplayLinkSetOutputCallback(displayLink!, displayLinkOutputCallback, UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()))
        

    }
    
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // calling the actual rendering-code from `gl-example`
        glExample.render()
        //  glFlush() is replaced with CGLFlushDrawable() and swaps the buffer being displayed
        CGLFlushDrawable(context.cglContextObj!)
        
    }
    
    override func reshape() {
        // calculating and setting the size of the rendering-area
        // `convertToBacking` calculates the actual pixel-size of the area, since on high-resolution displays there is
        // a difference betwenn the pixel-metric and _real_ pixels.
        let backingBounds = convertToBacking(self.bounds);
        glExample.setViewport(Int32.init(backingBounds.size.width), height: Int32.init(backingBounds.size.height))
    }

    
    func resume() {
        CVDisplayLinkStart(displayLink!)
    }
    
    func pause() {
        CVDisplayLinkStop(displayLink!)
    }
}
