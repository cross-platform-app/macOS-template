import Cocoa

/// `OnChangeListener` for the `CounterController`.
/// calls back to the UI-Thread when a state-change has occurred on the counter.
class OnCounterChangeListener: CROnChangeListener {
    private var counterController: CounterController!
    
    /// - parameters:
    ///     - counterController:    the controller-instance on which the UI-updates
    ///                             should be performed.
    init(counterController: CounterController!) {
        self.counterController = counterController
    }
    
    func onSuccess() {
        DispatchQueue.main.async {
            self.counterController.setSateCounterCanInteract(canInteract: true)
            self.counterController.showCounterError(show: false)
            self.counterController.updateCounter()
        }
    }
    
    func onError() {
        DispatchQueue.main.async {
            self.counterController.setSateCounterCanInteract(canInteract: true)
            self.counterController.showCounterError(show: true)
        }
    }
}


/// Controller for the counter-example.
/// This example shows how to interact with a C++-based library in an asynchronous matter.
class CounterController: NSViewController {
    private var counter: CRCounterInterface!
    @IBOutlet weak var textFieldCounter: NSTextField!
    @IBOutlet weak var buttonIncreaseCounter: NSButton!
    @IBOutlet weak var buttonResetCounter: NSButton!
    @IBOutlet weak var textFieldCounterError: NSTextField!
    @IBOutlet weak var progressIndicatorCounter: NSProgressIndicator!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Initializing the counter and registering a `OnChangeListener` to get informed on state-changes
        /// it is also possible to register more than one `OnChangeListener`!
        counter = CRCounterInterface.counter()
        counter.onNumberChanged(OnCounterChangeListener(counterController: self))

    }

    /// Updates the textField in the User-Interface that tells the current
    /// state of the counter. Call this when `onSuccess()` has been called in an `CROnChangeListener`
    /// registered in `counter`.
    public func updateCounter() {
        textFieldCounter.stringValue = String(counter.getNumber())
    }
    
    /// Shows and hides the error message that tells something went
    /// wrong when increasing the counter.
    /// - parameters:
    ///     - show: if `true`, the error message is made visible, if `false`, the error message is hidden
    public func showCounterError(show: Bool) {
        textFieldCounterError.isHidden = !show
    }
    
    /// Sets the state of the UI-elements. The elements can have two different states.
    /// - parameters:
    ///     - canInteract:
    ///         - `true`:   the user can interact with the elements. This means that he can click
    ///                     the _Increase Counter_-Button and the _Reset_-Button.
    ///         - `false`:  the user is not allowed to interact with the elements, because the last
    ///                     asynchronous state-change has not yet called back to the UI. This means
    ///                     that the buttons can not be clicked and a rotating progress-indicatior
    ///                     is showing up.
    public func setSateCounterCanInteract(canInteract: Bool) {
        if(canInteract) {
            buttonResetCounter.isEnabled = true
            buttonIncreaseCounter.isEnabled = true
            progressIndicatorCounter.isHidden = true
            progressIndicatorCounter.stopAnimation(self)
        } else {
            buttonResetCounter.isEnabled = false
            buttonIncreaseCounter.isEnabled = false
            progressIndicatorCounter.isHidden = false
            progressIndicatorCounter.startAnimation(self)
        }
        
    }
    
    /// Gets called by the _Increase Counter_-Button in the UI when the user clicks the button.
    @IBAction func onButtonIncreaseCounterClicked(_ sender: NSButton) {
        setSateCounterCanInteract(canInteract: false)
        DispatchQueue.global(qos: .userInitiated).async {
            self.counter.increaseNumber()
        }
        
    }
    
    /// Gets called by the _Reset_-Button in the UI when the user clicks the button.
    @IBAction func onButtonResetCounterClicked(_ sender: NSButton) {
        setSateCounterCanInteract(canInteract: false)
        DispatchQueue.global(qos: .userInitiated).async {
            self.counter.resetNumber()
        }
    }
}
